#!/bin/bash
ROOT_UID=0
SUCCESS=0

show_users(){
	count=0
	while IFS=: read -r f1 f2 f3 f4 f5 f6 f7
	do
		count=$(($count + 1))
		echo "Usuario[$count]"
		echo -e "\t Username: [\e[34m${f1}\e[0m]"
		echo -e "\t password: [\e[34m${f2}\e[0m]"
		echo -e "\t User_ID: [\e[34m${f3}\e[0m]"
		echo -e "\t Group_ID: [\e[34m${f4}\e[0m]"
		echo -e "\t Full_Name: [\e[34m${f5}\e[0m]"
		echo -e "\t ../: [\e[34m${f6}\e[0m]"
		echo -e "\t ../Shell: [\e[34m${f7}\e[0m]"
		sleep 1s
	done < ${file}
}

create_user(){
	useradd -p ${f2} -u ${f3} -g ${f4} -c "${f5}" -d ${f6} -s ${f7} ${f1}
	if [ $? -eq $SUCCESS ];
	then
		echo "Usuario [${f1}] agregado correctamente..."
	else
		echo "Usuario [${f1}] No se pudo agregar..."
	fi
}

if [ "$UID" -ne "$ROOT_UID" ]