#!/bin/bash
ROOT_UID=0
SUCCESS=0

# Run as root, of course. (this might not be necessary, because we have to run the script somehow with root anyway)
if [ "$UID" -ne "$ROOT_UID" ]
then
  echo "Ejecute como root este script"
  exit $E_NOTROOT
fi  

file=$1

if [ "${file}X" = "X" ];
then
   echo "Debe pasar como parametro el archivo donde estan los grupos"
   exit 1
fi

# Del archivo con el listado de usuarios a eliminar:
# Este es el formato:
# ejemplo
#    |   
#    f1  
#$f1 = username

eliminarGrupo(){
	#echo "Eliminar Usuario"
	
	#echo "Username 		  = ${user}"


	groupdel "${user}"
	if [ $? -eq $SUCCESS ];
	then
		echo "Usuario [${user}] eliminado correctamente..."
	else
		echo "Usuario [${user}] No se pudo eliminar..."
	fi
}

while IFS=: read -r f1
do
	eliminarGrupo "\${f1}"	
done < ${file}

exit 0